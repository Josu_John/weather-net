﻿using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Weather.Net.Core.Services;
using FluentAssertions;
using System.Threading.Tasks;

namespace Weather.Net.Core.Tests.Services
{
    [TestFixture]
    public class ForcastServiceTest
    {
        private string VALID_RESPONSE = @"{
  ""list"": [
    {
      ""main"": {
        ""temp"": 285.96,
        ""temp_min"": 285.96,
        ""temp_max"": 288.003
      },
      ""weather"": [
        {
          ""description"": ""light rain"",
          ""icon"": ""10n""
        }
      ],
      ""clouds"": { ""all"": 92 },
      ""wind"": {
        ""speed"": 1.21,
        ""deg"": 71.5023
      },
      ""dt_txt"": ""2017-09-25 00:00:00""
    }
  ]
};";
        private IForcastService _forcastService;
        private Mock<IHttpClientHelperService> _httpClientHelperServiceMock;
        [SetUp]
        public void Setup()
        {
            var appsettingsMock = new Mock<IOptions<AppSettings>>();
            appsettingsMock.Setup(s => s.Value).Returns(new AppSettings { });

            _httpClientHelperServiceMock = new Mock<IHttpClientHelperService>();
            _forcastService = new ForcastService(appsettingsMock.Object, _httpClientHelperServiceMock.Object);
        }

        [Test]
        public void Get_Returns_Null_For_Invalid_Response()
        {
            _httpClientHelperServiceMock.Setup(s => s.ReadAsync(It.IsAny<string>())).Returns(() => null);
            var result = _forcastService.GetForcast();
            result.ShouldBeEquivalentTo(null);
        }


        [Test]
        public async Task Get_Returns_2_Items_For_Valid_Response()
        {
            _httpClientHelperServiceMock.Setup(s => s.ReadAsync(It.IsAny<string>())).ReturnsAsync(() => VALID_RESPONSE
            );
            var result = await _forcastService.GetForcast();
            result.Should().HaveCount(2);
        }

        [Test]
        public async Task Get_Returns_With_Valid_Field_Values()
        {
            _httpClientHelperServiceMock.Setup(s => s.ReadAsync(It.IsAny<string>())).ReturnsAsync(() => VALID_RESPONSE
            );
            var result = await _forcastService.GetForcast();
            result[0].Temperature.ShouldBeEquivalentTo<Decimal>(285.96);
            result[0].MinTemperature.ShouldBeEquivalentTo<Decimal>(285.96);
            result[0].MaxTemperature.ShouldBeEquivalentTo<Decimal>(288.003);
            result[0].Icon.ShouldBeEquivalentTo("10n");
        }
    }
}
