﻿
namespace Weather.Net.Core.Models
{
    using System;

    public class ForcastModel
    {
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public decimal MinTemperature { get; set; }
        public decimal MaxTemperature { get; set; }
        public decimal Temperature { get; set; }
        public string City { get; set; }
    }
}
