﻿namespace Weather.Net.Core.Services
{
    using System.Reflection;
    using Autofac;
    using Module = Autofac.Module;

    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var types = typeof(ServiceModule).GetTypeInfo().Assembly.GetTypes();

            // register services
            builder.RegisterTypes(types)
                .Where(t => t.Namespace != null && t.Namespace.Contains(nameof(Weather.Net.Core.Services)))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
