﻿

namespace Weather.Net.Core.Services
{
    using System.Threading.Tasks;

    public interface IHttpClientHelperService
    {
         Task<string> ReadAsync(string url);
    }
}
