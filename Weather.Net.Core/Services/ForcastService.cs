﻿

namespace Weather.Net.Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Weather.Net.Core.Services;
    using Microsoft.Extensions.Options;
    using System.Net.Http;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Weather.Net.Core.Models;
    using System.Threading.Tasks;

    public class ForcastService :IForcastService
    {
        private readonly AppSettings _appSettings;
        private readonly IHttpClientHelperService _httpClientHelperService;


        public ForcastService(IOptions<AppSettings> options, IHttpClientHelperService httpClientHelperService)
        {
            _appSettings = options.Value;
            _httpClientHelperService = httpClientHelperService;
        }
        public async Task<List<ForcastModel>>  GetForcast(string country, string city)
        {

            var forcastList = new List<ForcastModel>();

            var httpClient = new HttpClient();
            var result = await _httpClientHelperService.ReadAsync(string.Format( _appSettings.OpenWeatherMapUrl,city, country)); 

            if(result is null)
            {
                return null;
            }

            var parsedResponse = JsonConvert.DeserializeObject<JObject>(result);
            var list = parsedResponse.SelectTokens("list");
            foreach (var item in list.Values())
            {
                forcastList.Add(new ForcastModel
                {
                    City = city,
                    Description = item.SelectToken("weather[0].description").Value<string>(),
                    Icon = item.SelectToken("weather[0].icon").Value<string>(),
                    Temperature = item.SelectToken("main.temp").Value<decimal>(),
                    MinTemperature = item.SelectToken("main.temp_min").Value<decimal>(),
                    MaxTemperature = item.SelectToken("main.temp_max").Value<decimal>(),
                    Date = item.SelectToken("dt_txt").Value<DateTime>(),
                });
            }


            return forcastList;
        }
    }
}
