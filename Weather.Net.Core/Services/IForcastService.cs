﻿

namespace Weather.Net.Core.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Weather.Net.Core.Models;

    public interface IForcastService
    {
        Task<List<ForcastModel>> GetForcast(string country , string city);
    }
}
