﻿

namespace Weather.Net.Core.Services
{
    using System.Net.Http;
    using System.Threading.Tasks;

    public class HttpClientHelperService : IHttpClientHelperService
    {
        public async Task<string> ReadAsync(string url)
        {
            var httpClient = new HttpClient();
            var result = httpClient.GetAsync(url).Result;

            if (result.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return null;
            }
            var contentString = await result.Content.ReadAsStringAsync();

            return contentString;
        }
    }
}
