﻿

namespace Weather.Net.Controllers
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Weather.Net.Core.Services;


    [Route("api/[controller]")]
    public class ForcastsController : Controller
    {
        private readonly IForcastService _weatherService;
        public ForcastsController(IForcastService weatherService)
        {
            _weatherService = weatherService;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IActionResult>  Get([FromQuery] string country, [FromQuery] string city)
        {
           var result=  await _weatherService.GetForcast(country,city);
            if(result is null)
            {
                return NotFound();
            }
            return new ObjectResult(result);
        }
       
    }
}
